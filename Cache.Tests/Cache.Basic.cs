using NUnit.Framework;
using Cache;

namespace Cache.Tests
{

    [TestFixture]
    public class Cache_Basic
    {
        private int[] IntArray(int i)
        {
            int[] ans = new int[i];
            for (int j=0; j<i; j++) 
            {
                ans[j]=i;
            }
            return ans;
        }

        private string Sum(int a,int b)
        {
            return $"Sum is {a + b}.";
        }


        [Test]
        public void Cache_YesMan()
        {
            Assert.Pass();
        }

        [Test]
        public void Cache_IntArray()
        {
            // IntArray(42) returns an int[42] where every element is 42.
            // intarray(42) should do the same, but using the cacheified IntArray

            var cache = new Cache<int,int[]>(IntArray,100);
            var intarray = cache.Proxy();
            
            int[] arr = intarray(42);
            Assert.IsTrue(arr.Length==42);
            Assert.IsTrue(arr[0]==42);
            
            arr = intarray(43);
            Assert.IsFalse(arr.Length==42);
            Assert.IsFalse(arr[0]==42);

            arr = intarray(42);
            Assert.IsTrue(arr.Length==42);
            Assert.IsTrue(arr[0]==42);

            Assert.IsTrue(cache.Hits() == 1);
            Assert.IsTrue(cache.Misses() == 2);
        }

        [Test]
        public void Cache_Sum()
        {
            // Sum(1,2) returns "Sum is 3."
            // sum(1,2) should do the same, but using the cacheified Sum

            var cache = new Cache<int,int,string>(Sum,100);
            var sum = cache.Proxy();

            Assert.IsTrue(sum(1,2)=="Sum is 3.");
            Assert.IsTrue(sum(10,20)=="Sum is 30.");
            Assert.IsTrue(sum(1,2)=="Sum is 3.");

            Assert.IsTrue(cache.Hits() == 1);
            Assert.IsTrue(cache.Misses() == 2);
        } // Cache_Sum

    } // class Cache_Basic

} // namespace 
