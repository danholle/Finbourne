﻿using System;
namespace Cache
{
    //
    //  In order to cache functions with different numbers of parameters, we have
    //  overloaded Cache classes for functions with 1..8 parameters.  Sorry.
    //

    /// <summary>
    /// Cacheify a function with 1 operand.
    /// </summary>
    /// <typeparam name="T1">Type of the operand.</typeparam>
    /// <typeparam name="TR">Type of the function result.</typeparam>
    public class Cache<T1,TR> where T1: notnull
    {
        private Func<T1,TR> _proxy;  
        private KVcache<T1,TR> _kvcache;

        /// <summary>
        /// Create a cache for a function with one operand.
        /// </summary>
        /// <param name="func">The function we're cacheifying.</param>
        /// <param name="maxCacheSize">The max number of cached results.  0=no limit</param>
        public Cache(Func<T1,TR> func, int maxCacheSize)
        {
            _kvcache = new KVcache<T1,TR>(func,maxCacheSize);
            _proxy = x => _kvcache.Get(x);
        }

        /// <summary>
        /// Produces a function which can be used exactly like original func, but has a cache.
        /// </summary>
        /// <returns>Cacheified func.</returns>
        public Func<T1,TR> Proxy() {return _proxy;}
        
        /// <summary>
        /// Provides the number of times the proxy returned a cached value, bypassing the func.
        /// </summary>
        /// <returns>Number of cache hits.</returns>
        public long Hits() { return _kvcache.hits; }

        /// <summary>
        /// Provides the number of times the proxy didn't find the answer in the cache, and had to invoke the func.
        /// </summary>
        /// <returns>Number of cache misses.</returns>
        public long Misses() { return _kvcache.misses; }
    } // Cache<InType,OutType>

    /// <summary>
    /// Cacheify a function with 2 operands.
    /// </summary>
    /// <typeparam name="T1">Type of the first operand.</typeparam>
    /// <typeparam name="T2">Type of the second operand.</typeparam>
    /// <typeparam name="TR">Type of the function result.</typeparam>
    public class Cache<T1,T2,TR> where T1: notnull
    {
        private Func<T1,T2,TR> _proxy;  
        private KVcache<(T1,T2),TR> _kvcache;

        /// <summary>
        /// Create a cache for a function with two operands.
        /// </summary>
        /// <param name="func">The function we're cacheifying.</param>
        /// <param name="maxCacheSize">The max number of cached results.  0=no limit</param> 
        public Cache(Func<T1,T2,TR> func, int maxCacheSize)
        {
            Func<(T1,T2),TR> unary = x => func(x.Item1,x.Item2);
            _kvcache = new KVcache<(T1,T2),TR>(unary,maxCacheSize);
            _proxy = (x1,x2) => _kvcache.Get((x1,x2));
        }

        /// <summary>
        /// Cacheified version of the func provided.
        /// </summary>
        /// <returns>A function that can be used just as the original func, but has a cache.</returns>
        public Func<T1,T2,TR> Proxy() {return _proxy;}

        /// <summary>
        /// Provides the number of times the proxy returned a cached value, bypassing the func.
        /// </summary>
        /// <returns>Number of cache hits.</returns>
        public long Hits() { return _kvcache.hits; }

        /// <summary>
        /// Provides the number of times the proxy didn't find the answer in the cache, and had to invoke the func.
        /// </summary>
        /// <returns>Number of cache misses.</returns>
        public long Misses() { return _kvcache.misses; }
    } 


} // namespace Cache

