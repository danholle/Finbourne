// 
// We have an expensive function, f.  Say it maps a parameter (SQL statement)
// into a result (an answer set).
//
// This class provides a slimmer, sexier version of f.  If it's been invoked
// recently with the same args, we return the answer immediately.
//
// This all relies on an LRU cache for that function, with a defined max
// number of values in that cache.
//
using System;
namespace Cache
{   
    /// <summary>
    /// A read-through key-value cache for a unary function Func&lt;K,V&gt;.
    /// </summary>
    /// <typeparam name="K">The type of the unary function's operand, and the key for the KV store.</typeparam>
    /// <typeparam name="V">The type of the unary function's result, and the value for the KV store.</typeparam>
    internal class KVcache<K,V> where K: notnull
    {
        private Dictionary<K, V> _cache;
        private LinkedList<K> _lruList;
        private Func<K,V> _func;
        private int _maxCacheSize;
        internal long hits;
        internal long misses;

        /// <summary>
        /// Constructor for KVcache.
        /// </summary>
        /// <param name="func">Function we are cacheing, with operand of type K and result of type V</param>
        /// <param name="maxCacheSize">Max number of &lt;key, value&gt; pairs in the LRU cache</param>
        public KVcache(Func<K,V> func, int maxCacheSize)
        {
            _cache = new Dictionary<K,V>();
            _lruList = new LinkedList<K>();
            _func = func;
            _maxCacheSize = maxCacheSize;
            hits = 0;
            misses = 0;
        }

        /// <summary>
        /// Store the value of _func(key) in the cache.
        /// </summary>
        /// <param name="key">Operand of _func.</param>
        /// <param name="value">Result of _func(key).</param>
        public void Set(K key, V value) 
        {
            if (_lruList.Contains(key))
            {
                _lruList.Remove(key);
            }

            if (_lruList.Count == _maxCacheSize)
            {
                // Remove the least recently used entry from the cache and the LRU list
                if (_lruList?.First is not null)
                {
                    K evictedKey = _lruList.First.Value;
                    _lruList.RemoveFirst();
                    _cache.Remove(evictedKey);
                }
            }

            _lruList?.AddLast(key);
            _cache[key] = value;
        } // method Set

        /// <summary>
        /// Gets the value of _func(key), checking the cache first.
        /// </summary>
        /// <param name="key">The operand of the function we're cacheing.</param>
        /// <returns>The value of _func(key), from cache or from _func.</returns>
        public V Get(K key)
        {
            if (_cache.ContainsKey(key))
            {
                hits += 1;

                // Update the LRU list to move this key to the end
                _lruList.Remove(key);
                _lruList.AddLast(key);

                return _cache[key];
            }
            else
            {
                misses += 1;

                // Retrieve the value from the expensive function and add it to the cache
                V value = _func(key);  // That expression seems to be funky?
                Set(key, value);
                return value;
            }
        } // method Get

    } // class KVcache

} // namespace Cache