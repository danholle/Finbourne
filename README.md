![banner](/images/cache_banner.jpg)

# Cache #

**Cache** provides a mechanism for augmenting an expensive function with 
a cache, with zero impact to the expensive function and minimal impact to the client application.

This is done by creating a proxy function that looks the same as the original (expensive) function, but is augmented with a cache under the covers. This means that if a set of parameters has a stored answer in the cache, it can be quickly returned without running the expensive function again. However, if there is no stored answer, the expensive function will be evaluated and the result will be stored in the cache for future use... all transparently.

This can help improve the performance of an application by reducing the need to run expensive functions multiple times for the same set of input parameters.

## Example

Your application runs lots of database queries, which tend to look something like this:
```csharp
    string sqltext = "select count(*) from transactions";
    DataTable a = db.RunQuery(conn, sqltext);
```

To speed this up, let's put a query cache in front of RunQuery.  The cache will keep up to 100 queries, with a least-recently-used (LRU) eviction policy.
```csharp
    var cache = new Cache<Connection,string,DataTable>(db.RunQuery,100);
```    
This creates a cache for db.RunQuery(Connection,String) which produces a DataTable result.  It keeps 100 results in a least-recently-used cache.

Now let's get a proxy for db.RunQuery which acts the same way, but uses our cache.
```csharp   
    var runquery = cache.Proxy();
```

Now all we have to do is replace db.RunQuery with runquery, like this:
```csharp   
    DataTable a = runquery(conn, sqltext);
```
and all that stuff is taken care of for us.


## Stuff I Haven't Done Yet

This is all working, but it's not really ready for prime time.
- *Testing.*  I have some basic tests in place in Cache.Tests, using nUnit (which I think is the Finbourne default) ... but there's much more that should be done.
- *Instrumenting.*  I'm only capturing cache hit and cache misses.  There is much more that can be done here.  Ideally, it should be easy to have stats which allow auto-tuning of the cache size.
- *Logging.*  Would be good to have an ability to enable logging (say, dumping stats every n calls, or something).
- *Documenting.*  I started adding XML comments.  Still haven't gotten the full picture of how XML comments are used in C#.  Looks like Intellisense is finding them, which is a start.
- *Validating.*  There are several places where parameter checking and the like would be the polite thing to do.
- *Concurrency.*  I haven't considered what it would take to make this thread safe.  I need to learn more about C# concurrency first.
- *Finbourne-specific coding conventions.*  I don't know what they are (yet).  I've tried to use standard C# style.
- *Exception handling.*  Should be raising exceptions for invalid requests.  If the function we're cacheing fails, it should get propagated up the call stack (or whatever's right) so it looks exactly like the un-cached behaviour
- *Working Examples.*  I set up a sub-project for this but haven't actioned it yet.
- ... and probably a bunch more.
